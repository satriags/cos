<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.

// ADMIN
$routes->get('/', 'Auth::index');
$routes->add('/auth/ceklogin/(:segment)/(:segment)', 'Auth::cekLogin/$1/$2');
$routes->add('/auth/tambahakun', 'Auth::createAccount');
$routes->add('/auth/akun', 'Auth::showAllData');
$routes->add('/auth/akun/(:segment)', 'Auth::showDetailData/$1');
$routes->add('/auth/akun/hapus/(:segment)', 'Auth::deleteAkunData/$1');
$routes->post('/auth/akun/edit', 'Auth::editAkunData');

$routes->add('/siswa/orangtua', 'Siswa::showAllDataGuru');
$routes->add('/siswa/buat', 'Siswa::createSiswa');
$routes->add('/siswa/edit', 'Siswa::updateSiswa');
$routes->add('/siswa/kirimfoto', 'Siswa::uploadImageSiswa');
$routes->add('/siswa/list', 'Siswa::showAllDataSiswa');
$routes->add('/siswa/detail/(:segment)', 'Siswa::showDetailSiswa/$1');
$routes->add('/siswa/hapus/(:segment)', 'Siswa::hapusSiswa/$1');

$routes->add('/kasus/gurulist', 'Pelanggaran::getGuru');
$routes->add('/kasus/ortulist', 'Pelanggaran::getOrtu');
$routes->add('/kasus/siswalist/(:segment)', 'Pelanggaran::getSiswa/$1');
$routes->post('/kasus/simpan', 'Pelanggaran::addCase');
$routes->add('/kasus/kasuslist', 'Pelanggaran::getCase');
$routes->add('/kasus/detail/(:num)', 'Pelanggaran::getDetailCase/$1');
$routes->add('/kasus/hapus/(:num)', 'Pelanggaran::removeCase/$1');

// GURU
$routes->add('/kasus/kasuslistid/(:num)', 'Pelanggaran::getCasebyID/$1');
$routes->add('/kasus/edit', 'Pelanggaran::EditbyID');
// GURU CHAT
$routes->add('/chat/listcase/(:num)', 'Chat::getChatByCase/$1');
$routes->add('/chat/detail/(:num)', 'Chat::getChatDetail/$1');
$routes->add('/chat/kirim', 'Chat::sendChat');
$routes->add('/chat/delete/(:num)', 'Chat::deleteChat/$1');
$routes->add('/chat/cekchat/(:num)', 'Chat::checkCountChat/$1');



/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
