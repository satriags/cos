<?php

namespace App\Controllers;

class Siswa extends BaseController
{
	public function uploadImageSiswa()
	{
		// header("Access-Control-Allow-Origin: *");
		// header("Access-Control-Allow-Methods: GET, POST, FILES");
		// header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

		// $HTTP_RAW_POST_DATA;
		$getName = $this->siswaModel->orderBy('id_siswa', 'DESC')->first();

		// if ($getName['created_at'] != $getName['updated_at']) {
		// 	unlink("img/siswa/" . $getName['foto']);
		// }
		$data = file_get_contents('php://input');

		// echo $_POST['tes'];
		// dd($_FILES);
		// dd($data);
		// echo 'tes';
		// exit;
		if (!(file_put_contents(WRITEPATH . 'uploads/img/siswa/' . $getName['foto'], $data) == false)) {
			echo 'Berhasil menyimpan data siswa';
		} else {
			echo 'Sepertinya ada masalah, silahkan hubungi admin';
		}
	}

	public function updateSiswa()
	{
		$namaOrtu = $this->request->getVar('nama_ortu');
		$noHP = $this->request->getVar('nohp_ortu');

		$cekIdOrtu = $this->ortuModel->where([
			'nama' => $namaOrtu,
			'no_telp' => $noHP
		])->first();
		$idOrtu = $cekIdOrtu['id_ortu'];
		if (!$idOrtu) {
			echo 'tidak dapat menemukan data orang tua';
			exit;
		}
		$nis_lama = $this->request->getVar('nis_lama');
		$nis = $this->request->getVar('nis');
		$nama = $this->request->getVar('nama');
		$tgllahir = $this->request->getVar('tgllahir');
		$tmplahir = $this->request->getVar('tmplahir');
		$jnskel = $this->request->getVar('jnskel');
		$agama = $this->request->getVar('agama');
		$alamat = $this->request->getVar('alamat');
		$no_telp = $this->request->getVar('no_telp');
		$kelas = $this->request->getVar('kelas');
		$tahun = $this->request->getVar('tahun');
		$anak_ke = $this->request->getVar('anak_ke');
		$foto = $this->request->getVar('foto');

		if ($foto != "") {

			$foto = 'foto_' . rand(10, 1000) . '_' . time() . '.jpg';
		}

		if ($foto != "") {

			$this->siswaModel->set([
				'id_ortu' => $idOrtu,
				'nis' =>  $nis,
				'nama' =>  $nama,
				'tgllahir' =>  $tgllahir,
				'tmplahir' =>  $tmplahir,
				'jnskel' => $jnskel,
				'agama' =>  $agama,
				'alamat' =>  $alamat,
				'no_telp' =>  $no_telp,
				'kelas' =>  $kelas,
				'tahun' =>  $tahun,
				'anak_ke' =>  $anak_ke,
				'foto' =>  $foto
			])->where('nis', $nis_lama)->update();
		} else {

			$this->siswaModel->set([
				'id_ortu' => $idOrtu,
				'nis' =>  $nis,
				'nama' =>  $nama,
				'tgllahir' =>  $tgllahir,
				'tmplahir' =>  $tmplahir,
				'jnskel' => $jnskel,
				'agama' =>  $agama,
				'alamat' =>  $alamat,
				'no_telp' =>  $no_telp,
				'kelas' =>  $kelas,
				'tahun' =>  $tahun,
				'anak_ke' =>  $anak_ke,
			])->where('nis', $nis_lama)->update();
		}
	}
	public function createSiswa()
	{
		$namaOrtu = $this->request->getVar('nama_ortu');
		$noHP = $this->request->getVar('nohp_ortu');

		$cekIdOrtu = $this->ortuModel->where([
			'nama' => $namaOrtu,
			'no_telp' => $noHP
		])->first();
		$idOrtu = $cekIdOrtu['id_ortu'];
		if (!$idOrtu) {
			echo 'tidak dapat menemukan data orang tua';
			exit;
		}
		$nis = $this->request->getVar('nis');
		$nama = $this->request->getVar('nama');
		$tgllahir = $this->request->getVar('tgllahir');
		$tmplahir = $this->request->getVar('tmplahir');
		$jnskel = $this->request->getVar('jnskel');
		$agama = $this->request->getVar('agama');
		$alamat = $this->request->getVar('alamat');
		$no_telp = $this->request->getVar('no_telp');
		$kelas = $this->request->getVar('kelas');
		$tahun = $this->request->getVar('tahun');
		$anak_ke = $this->request->getVar('anak_ke');
		$foto = 'foto_' . rand(10, 1000) . '_' . time() . '.jpg';


		$cekNIS = $this->siswaModel->where([
			'nis' =>  $nis,
		])->first();
		$cekHP = $this->siswaModel->where([
			'no_telp' =>  $no_telp,
		])->first();

		if ($cekNIS) {
			echo 'gagalNIS';
		} elseif ($cekHP) {
			echo 'gagalHP';
		} else {
			$this->siswaModel->save([
				'id_ortu' => $idOrtu,
				'nis' =>  $nis,
				'nama' =>  $nama,
				'tgllahir' =>  $tgllahir,
				'tmplahir' =>  $tmplahir,
				'jnskel' => $jnskel,
				'agama' =>  $agama,
				'alamat' =>  $alamat,
				'no_telp' =>  $no_telp,
				'kelas' =>  $kelas,
				'tahun' =>  $tahun,
				'anak_ke' =>  $anak_ke,
				'foto' =>  $foto
			]);
			echo 'progres foto';
		}
	}
	public function showAllDataGuru()
	{
		$cek = $this->ortuModel->orderBy('nama', 'ASC')->get()->getResultArray();
		foreach ($cek as $d) {
			echo $d['nama'] . " - ";
			echo $d['no_telp'] . ",";
		}
	}
	public function hapusSiswa($nis)
	{
		$cekSiswa = $this->siswaModel->where('nis', $nis)->first();
		$cekCases = $this->pelanggaranModel->where('id_siswa', $cekSiswa['id_siswa'])->first();
		unlink("img/siswa/" . $cekSiswa['foto']);

		$this->chatModel->where('id_pelanggaran', $cekCases['id_pelanggaran'])->delete();
		$this->pelanggaranModel->where('id_siswa', $cekSiswa['id_siswa'])->delete();
		$this->siswaModel->where('id_siswa', $cekSiswa['id_siswa'])->delete();

		echo 'berhasil hapus siswa';
	}
	public function showAllDataSiswa()
	{
		$data = $this->siswaModel->select('nis,nama')->orderBy('nama', 'ASC')->get()->getResultArray();
		foreach ($data as $d) {

			echo $d['nama'] . " - " . $d['nis'] . ",";
		}
	}

	public function showDetailSiswa($nis)
	{
		$data = $this->siswaModel->select('id_ortu,nis,nama,tgllahir,tmplahir,jnskel,agama,alamat,no_telp,kelas,tahun,anak_ke,CONCAT("' . base_url() . '/img/siswa/",foto)')->where('nis', $nis)->first();
		$data2 = $this->ortuModel->select('nama,no_telp')->where('id_ortu', $data['id_ortu'])->first();
		echo trim(implode(',', $data)) . ",";
		echo trim(implode(',', $data2));
	}
}
