<?php

namespace App\Controllers;

class Chat extends BaseController
{
    public function getChatByCase($idLog)
    {
        // CEK TIPE AKUN
        $cekAkun = $this->userAkunModel->where('id_user', $idLog)->first();

        if ($cekAkun['type'] == 'BK' || $cekAkun['type'] == 'Guru') {
            $getData = $this->db->query("SELECT
                    p.judul,CONCAT(s.nama,' (Orang Tua : ',o.nama,')') as x,s.foto,p.id_pelanggaran,g.nama, 
                    -- UNTUK DETAIL CASE
                    p.judul as 'judulkasus',p.deskripsi as 'deskkasus',s.nama as 'namasiswa',o.nama as 'namaortu', g.nama as 'namaguru',s.foto as 'filefoto',p.kategori as 'kategorikasus',IF(p.status = '0','Kasus Terbuka','Kasus Ditutup') as 'statuskasus',(SELECT type FROM user WHERE id_user = g.id_user) as 'typeakunguru'
                FROM
                    pelanggaran p
                LEFT JOIN siswa s ON p.id_siswa = s.id_siswa
                LEFT JOIN guru g ON p.id_guru = g.id_guru
                LEFT JOIN ortu o ON p.id_ortu = o.id_ortu
                WHERE g.id_user = {$idLog}")->getResultArray();


            // foreach ($getData as $d) {
            //     echo $d['judul'] . "," . $d['x']  . "," . $d['status'] . "," . $d['id_pelanggaran'] . "," . $d['nama']     . "," . $cekAkun['type'] . "|";
            // }
        } elseif ($cekAkun['type'] == 'Ortu') {

            $getData = $this->db->query("SELECT
                    p.judul,CONCAT(s.nama,' (Guru : ',g.nama,')') as x,s.foto,p.id_pelanggaran,o.nama,
                -- UNTUK DETAIL CASE
                    p.judul as 'judulkasus',p.deskripsi as 'deskkasus',s.nama as 'namasiswa',o.nama as 'namaortu', g.nama as 'namaguru',s.foto as 'filefoto',p.kategori as 'kategorikasus',IF(p.status = '0','Kasus Terbuka','Kasus Ditutup') as 'statuskasus',(SELECT type FROM user WHERE id_user = g.id_user) as 'typeakunguru'
                FROM
                    pelanggaran p
                LEFT JOIN siswa s ON p.id_siswa = s.id_siswa
                LEFT JOIN guru g ON p.id_guru = g.id_guru
                LEFT JOIN ortu o ON p.id_ortu = o.id_ortu
                WHERE o.id_user = {$idLog}")->getResultArray();
        }
        foreach ($getData as $d) {
            echo
            // 1
            "Pelanggaran : " . $d['judul'] .
                // 2
                "|" . $d['x']  .
                // 3
                "|" . $d['foto'] .
                // 4
                "|" . $d['id_pelanggaran'] .
                // 5
                "|" . $d['nama']     .
                // 6
                "|" . $cekAkun['type'] .
                // 7
                "|" . $d['judulkasus'] .
                // 8
                "|" . $d['deskkasus'] .
                // 9
                "|" . $d['namasiswa'] .
                // 10
                "|" . $d['namaortu'] .
                // 11
                "|" . $d['namaguru'] .
                // 12
                "|" . base_url() . "/img/siswa/" . $d['filefoto'] .
                //13
                "|" . $d['kategorikasus'] .
                //14
                "|" . $d['statuskasus'] .
                // 15
                "|" . $d['typeakunguru'] .
                "||";
        }
    }

    public function getChatDetail($idPelanggaran)
    {
        $getChat = $this->db->query("SELECT
                CONCAT(IF(c.id_user = g.id_user,g.nama,o.nama),' (',u.username,')') as x, c.chat, c.created_at,u.id_user,c.id_chat
                FROM
                    chat c
                LEFT JOIN user u ON
                    c.id_user = u.id_user
                LEFT JOIN guru g ON
                    g.id_user = c.id_user
                LEFT JOIN ortu o ON
                    o.id_user = c.id_user
                WHERE
                    c.id_pelanggaran = {$idPelanggaran}
                ORDER BY
                    c.id_chat ASC")->getResultArray();

        foreach ($getChat as $s) {
            echo $s['x'] . "|" . $s['chat'] . "|" . $this->formatDate($s['created_at']) . "  (" . date_format(date_create($s['created_at']), "g:i A") . ")|" . $s['id_user'] .  "|" . $s['id_chat'] . '||';
        }
    }
    public function checkCountChat($id_akun)
    {
        // CEK TIPE AKUN
        $cekAkun = $this->userAkunModel->where('id_user', $id_akun)->first();

        if ($cekAkun['type'] == 'BK' || $cekAkun['type'] == 'Guru') {
            $sql = $this->db->query("SELECT COUNT(*) as 'totalchatnow'
                FROM
                    chat c
                LEFT JOIN user u ON
                    c.id_user = u.id_user
                LEFT JOIN guru g ON
                    g.id_user = c.id_user
                LEFT JOIN ortu o ON
                    o.id_user = c.id_user
                WHERE
                    c.id_user = {$cekAkun['id_user']}
                ORDER BY
                    c.id_chat ASC")->getResultArray();
        } elseif ($cekAkun['type'] == 'Ortu') {
            $sql = $this->db->query("SELECT COUNT(*) as 'totalchatnow'
                FROM
                    chat c
                LEFT JOIN user u ON
                    c.id_user = u.id_user
                LEFT JOIN guru g ON
                    g.id_user = c.id_user
                LEFT JOIN ortu o ON
                    o.id_user = c.id_user
                WHERE
                    c.id_user = {$cekAkun['id_user']}
                ORDER BY
                    c.id_chat ASC")->getResultArray();
        }
        echo $sql[0]['totalchatnow'];
    }
    public function deleteChat($id_cht)
    {
        $cek = $this->chatModel->delete($id_cht);
        if ($cek) {
            echo 'berhasil menghapus pesan';
        } else {
            echo 'sepertinya terjadi sesuatu saat melakukan aksi ini';
        }
    }

    public function sendChat()
    {
        $id_pelanggaran = $this->request->getVar('id_pelanggaran');
        $id_pengirim = $this->request->getVar('id_pengirim');
        $pesan = $this->request->getVar('pesan');

        $cek = $this->chatModel->save([
            'id_user' => $id_pengirim,
            'id_pelanggaran' => $id_pelanggaran,
            'chat' => $pesan
        ]);
        if ($cek) {
            echo 'nice';
        } else {
            echo 'seperti terjadi kesalahan saat mengirim pesan';
        }
    }


    function formatDate($date)
    {
        $cek = ltrim(date_format(date_create($date), "m"), 0);

        $bulan = array(
            '1' => 'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        return date_format(date_create($date), "d ") . $bulan[$cek] . date_format(date_create($date), " Y");
    }
}
