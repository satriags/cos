<?php

namespace App\Controllers;

class Auth extends BaseController
{
	public function cekLogin($username, $password)
	{
		$us = $username;
		$pw = md5(md5($password));
		$cek = $this->userAkunModel->select('`id_user`, `username`, `password`, `type`, `created_at`')->where(['username' => $us, 'password' => $pw])->first();

		if ($cek != null) {

			if ($cek['type'] == 'Admin') {

				echo implode('|', $cek);
			} elseif ($cek['type'] == 'Guru' || $cek['type'] == 'BK') {
				$cek2 = $this->guruModel->select('`id_guru`, `nama`, `wali_kelas`, `jnskel`, `mapel`, `no_telp`, `jabatan`')->where('id_user', $cek['id_user'])->first();
				$data = implode('|', $cek);
				$data .= "|" . implode('|', $cek2);
				echo $data;
			} elseif ($cek['type'] == 'Ortu') {
				$cek2 = $this->ortuModel->select('`id_ortu`, `nama`, `no_telp`, `alamat`, `pekerjaan`')->where('id_user', $cek['id_user'])->first();
				$data = implode('|', $cek);
				$data .= "|" . implode('|', $cek2);
				echo $data;
			}
		} else {
			echo '0';
		}
	}

	public function showAllData()
	{
		$cek = $this->userAkunModel->select('username')->where('username !=', 'admincos')->orderBy('username', 'ASC')->get()->getResultArray();
		foreach ($cek as $d) {
			echo $d['username'] . ",";
		}
	}

	public function editAkunData()
	{

		$username = $this->request->getVar('username');
		$password = ($this->request->getVar('password') == "") ? '' : md5(md5($this->request->getVar('password')));
		$type = $this->request->getVar('type');

		$notif_pw = '';
		// 			Guru
		// 			Ortu
		// 			Admin
		// BK
		if ($password != '') {

			$changePW = $this->userAkunModel->set('password', $password)->where('username', $username)->update();
			$notif_pw = " dan ubah password akun";
		} else if ($type == 'Admin') {
			echo 'Berhasil ubah password admin';
		}


		$getId = $this->userAkunModel->where('username', $username)->first();

		if ($type === 'BK' || $type === 'Guru') {

			$nama = $this->request->getVar('nama');
			$wali_kelas = $this->request->getVar('wali_kelas');
			$jnskel = $this->request->getVar('jnskel');
			$mapel = $this->request->getVar('mapel');
			$no_telp = $this->request->getVar('no_telp');
			$jabatan = $this->request->getVar('jabatan');

			$cekCreateGuru = $this->guruModel->set([
				'nama' => $nama,
				'wali_kelas' => $wali_kelas,
				'jnskel' => $jnskel,
				'mapel' => $mapel,
				'no_telp' => $no_telp,
				'jabatan' => $jabatan
			])->where('id_user', $getId['id_user'])->update();


			if (!$cekCreateGuru) {

				echo 'Nomor HP telah terdaftar, gunakan nomor HP lainnya';
			} else {
				echo 'Berhasil ubah akun Guru' . $notif_pw;
			}
		} elseif ($type === 'Ortu') {
			$nama = $this->request->getVar('nama');
			$no_telp = $this->request->getVar('no_telp');
			$alamat = $this->request->getVar('alamat');
			$pekerjaan = $this->request->getVar('pekerjaan');

			$cekCreateOrtu = $this->ortuModel->set([
				'nama' => $nama,
				'no_telp' => $no_telp,
				'alamat' => $alamat,
				'pekerjaan' => $pekerjaan
			])->where('id_user', $getId['id_user'])->update();

			if (!$cekCreateOrtu) {

				echo 'Nomor HP telah terdaftar, gunakan nomor HP lainnya';
			} else {
				echo 'Berhasil membuat akun orang tua' . $notif_pw;
			}
		}
	}
	public function deleteAkunData($username)
	{

		$cek = $this->userAkunModel->where('username', $username)->first();


		$this->chatModel->where('id_user', $cek['id_user'])->delete();

		if ($cek['type'] == 'Ortu') {
			$cek2 = $this->ortuModel->where('id_user', $cek['id_user'])->first();

			$this->pelanggaranModel->where('id_ortu', $cek2['id_ortu'])->delete();
			$this->siswaModel->where('id_ortu', $cek2['id_ortu'])->delete();
			$this->ortuModel->delete($cek2['id_ortu']);
			$this->userAkunModel->delete($cek['id_user']);

			echo 'berhasil hapus akun Orang Tua';
		} elseif ($cek['type'] == 'BK' || $cek['type'] == 'Guru') {

			$cek2 = $this->guruModel->where('id_user', $cek['id_user'])->first();
			$this->pelanggaranModel->where('id_guru', $cek2['id_guru'])->delete();
			$this->guruModel->delete($cek2['id_guru']);
			$this->userAkunModel->delete($cek['id_user']);

			echo 'berhasil hapus akun Guru';
		} else {
			$this->userAkunModel->delete($cek['id_user']);

			echo 'berhasil hapus akun Admin';
		}
	}


	public function showDetailData($username)
	{
		$cek = $this->userAkunModel->select('username,type,id_user')->where('username', $username)->first();
		if ($cek['type'] == 'Admin') {

			echo implode('|', $cek);
		} elseif ($cek['type'] == 'Guru' || $cek['type'] == 'BK') {
			$cek2 = $this->guruModel->select('`nama`, `wali_kelas`, `jnskel`, `mapel`, `no_telp`, `jabatan`,`id_guru`')->where('id_user', $cek['id_user'])->first();
			$data = implode('|', $cek);
			$data .= "|" . implode('|', $cek2);
			echo $data;
		} elseif ($cek['type'] == 'Ortu') {
			$cek2 = $this->ortuModel->select('`nama`, `no_telp`, `alamat`, `pekerjaan`,`id_ortu`')->where('id_user', $cek['id_user'])->first();
			$data = implode('|', $cek);
			$data .= "|" . implode('|', $cek2);
			echo $data;
		}
	}

	public function createAccount()
	{
		$username = $this->request->getVar('username');
		$password = md5(md5($this->request->getVar('password')));
		$type = $this->request->getVar('type');

		// 			Guru
		// 			Ortu
		// 			Admin
		// BK

		$cekCreate = $this->userAkunModel->save([
			'username' => $username,
			'password' => $password,
			'type' => $type
		]);

		if (!$cekCreate) {

			echo 'username telah terdaftar, gunakan username lainnya!';
			exit;
		} else if ($type != 'BK' && $type != 'Guru' && $type != 'Ortu') {
			echo 'Berhasil melakukan pendaftaran akun Admin';
		}

		$getId = $this->userAkunModel->orderBy('id_user', 'DESC')->first();

		if ($type === 'BK' || $type === 'Guru') {

			$nama = $this->request->getVar('nama');
			$wali_kelas = $this->request->getVar('wali_kelas');
			$jnskel = $this->request->getVar('jnskel');
			$mapel = $this->request->getVar('mapel');
			$no_telp = $this->request->getVar('no_telp');
			$jabatan = $this->request->getVar('jabatan');

			$cekCreateGuru = $this->guruModel->save([
				'id_user' => $getId['id_user'],
				'nama' => $nama,
				'wali_kelas' => $wali_kelas,
				'jnskel' => $jnskel,
				'mapel' => $mapel,
				'no_telp' => $no_telp,
				'jabatan' => $jabatan
			]);


			if (!$cekCreateGuru) {

				$this->userAkunModel->delete($getId['id_user']);
				echo 'Nomor HP telah terdaftar, gunakan nomor HP lainnya';
			} else {
				echo 'Berhasil membuat akun Guru';
			}
		} elseif ($type === 'Ortu') {
			$nama = $this->request->getVar('nama');
			$no_telp = $this->request->getVar('no_telp');
			$alamat = $this->request->getVar('alamat');
			$pekerjaan = $this->request->getVar('pekerjaan');

			$cekCreateOrtu = $this->ortuModel->save([
				'id_user' => $getId['id_user'],
				'nama' => $nama,
				'no_telp' => $no_telp,
				'alamat' => $alamat,
				'pekerjaan' => $pekerjaan
			]);

			if (!$cekCreateOrtu) {

				$this->userAkunModel->delete($getId['id_user']);
				echo 'Nomor HP telah terdaftar, gunakan nomor HP lainnya';
			} else {
				echo 'Berhasil membuat akun orang tua';
			}
		}
	}
}
