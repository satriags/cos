<?php

namespace App\Controllers;

class Pelanggaran extends BaseController
{
	public function getGuru()
	{
		$get = $this->db->query('SELECT 
CONCAT(o.nama," - ",o.no_telp," - ",u.type)
FROM guru o
LEFT JOIN user u ON o.id_user = u.id_user')->getResultArray();
		foreach ($get as $s) {
			echo implode(",", $s) . ",";
		}
	}
	public function getOrtu()
	{


		$get = $this->ortuModel->select('concat(nama," - ",no_telp)')->get()->getResultArray();
		foreach ($get as $s) {
			echo implode(",", $s) . ",";
		}
	}
	public function getSiswa($ortuHP)
	{
		$get = $this->ortuModel->where('no_telp', trim($ortuHP))->first();

		$get2 = $this->siswaModel->select('concat(nama," - ",nis)')->where('id_ortu', $get['id_ortu'])->get()->getResultArray();
		foreach ($get2 as $s) {
			echo implode(",", $s) . ",";
		}
	}
	public function addCase()
	{
		$guruhp = $this->request->getVar('guruhp');
		$cekguruHP = $this->guruModel->where('no_telp', $guruhp)->first();

		$siswahp = $this->request->getVar('siswahp');
		$ceksiswaHP = $this->siswaModel->where('nis', $siswahp)->first();

		$ortuhp = $this->request->getVar('ortuhp');
		$cekortuHP = $this->ortuModel->where('no_telp', $ortuhp)->first();

		$judul = $this->request->getVar('judul');
		$deskripsi = $this->request->getVar('deskripsi');
		$kat = $this->request->getVar('kat');


		$cek = $this->pelanggaranModel->save([
			'id_siswa' => $ceksiswaHP['id_siswa'],
			'id_guru' => $cekguruHP['id_guru'],
			'id_ortu' => $cekortuHP['id_ortu'],
			'judul' => $judul,
			'kategori' => $kat,
			'deskripsi' => $deskripsi,
			'tanggal' => $this->formatDate(date('Y-m-d')),
		]);
		if ($cek) {
			echo 'berhasil menyimpan kasus';
		} else {
			echo 'gagal simpan, sepertinya terjadi sesuatu dalam menyimpan pelanggan, hubungi admin!';
		}
	}

	public function removeCase($id_pel)
	{
		$cek1 = $this->chatModel->where('id_pelanggaran', $id_pel)->delete();
		$cek2 = $this->pelanggaranModel->delete($id_pel);

		if ($cek2) {
			echo 'berhasil hapus pelanggaran dan seluruh chat';
		} elseif ($cek1) {
			echo 'hanya berhasil hapus pelanggaran';
		} else {
			echo 'Gagal menghapus';
		}
	}

	public function getCase()
	{
		$cek = $this->db->query('SELECT CONCAT((p.id_pelanggaran)," - ",p.judul) as a FROM pelanggaran p LEFT JOIN siswa s ON p.id_siswa = s.id_siswa LEFT JOIN guru g ON p.id_guru = g.id_guru LEFT JOIN ortu o ON p.id_ortu = o.id_ortu ORDER BY p.id_pelanggaran DESC')->getResultArray();
		foreach ($cek as $a) {
			echo $a['a'] . ",";
		}
	}

	public function getCasebyID($id_guru)
	{
		$cek = $this->db->query('SELECT CONCAT((p.id_pelanggaran)," - ",p.judul) as a FROM pelanggaran p LEFT JOIN siswa s ON p.id_siswa = s.id_siswa LEFT JOIN guru g ON p.id_guru = g.id_guru LEFT JOIN ortu o ON p.id_ortu = o.id_ortu WHERE g.id_guru = ' . $id_guru . ' ORDER BY p.id_pelanggaran DESC')->getResultArray();
		foreach ($cek as $a) {
			echo $a['a'] . ",";
		}
	}

	public function EditbyID()
	{
		$id_kasus = $this->request->getVar('id_kasus');
		$judul = $this->request->getVar('judul');
		$deskripsi = $this->request->getVar('deskripsi');
		$kat = $this->request->getVar('kat');
		$st = $this->request->getVar('statusnya');
		// $st = ($this->request->getVar('status') == 'Kasus Terbuka') ? 0 : 1;

		$cekupd =  $this->pelanggaranModel->set([
			'judul' => $judul,
			'kategori' => $kat,
			'deskripsi' => $deskripsi,
			'status' => $st
		])->update($id_kasus);

		if ($cekupd) {
			echo 'berhasil melakukan update data pelanggaran';
		} else {
			echo 'sepertinya terdapat masalah pada perubahan pelanggaran';
		}
	}

	public function getDetailCase($id)
	{
		$cek = $this->db->query("SELECT CONCAT(g.nama,'|',o.nama,'|',s.nama,'|',p.judul,'|',p.deskripsi,'|',p.kategori,'|',p.status) as a FROM pelanggaran p LEFT JOIN siswa s ON p.id_siswa = s.id_siswa LEFT JOIN guru g ON p.id_guru = g.id_guru LEFT JOIN ortu o ON p.id_ortu = o.id_ortu WHERE p.id_pelanggaran = '$id'")->getResultArray();

		echo $cek[0]['a'];
	}

	function formatDate($date)
	{
		$cek = ltrim(date_format(date_create($date), "m"), 0);

		$bulan = array(
			'1' => 'Januari',
			'Februari',
			'Maret',
			'April',
			'Mei',
			'Juni',
			'Juli',
			'Agustus',
			'September',
			'Oktober',
			'November',
			'Desember'
		);
		return date_format(date_create($date), "d ") . $bulan[$cek] . date_format(date_create($date), " Y");
	}
}
