<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class UserSeeder extends Seeder
{
	public function run()
	{
		$data = [
			[
				'username' => 'admincos',
				'password' => 'c3284d0f94606de1fd2af172aba15bf3',  // admin
				'type' => 'Admin',
			]
		];

		// Simple Queries

		// $this->db->query("INSERT INTO staff (nama, jabatan,foto,nohp,email,password) VALUES(:nama:,:jabatan:,:foto:,:nohp:,:email:,:password:)", $data);

		// Using Query Builder
		$this->db->table('user')->insertBatch($data);
	}
}
