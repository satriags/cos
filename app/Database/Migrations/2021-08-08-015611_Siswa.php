<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Siswa extends Migration
{
	public function up()
	{
		// Membuat kolom/field untuk tabel news
		$this->forge->addField([
			'id_siswa' => [
				'type'           => 'INT',
				'constraint'     => 11,
				'auto_increment' => true
			],
			'id_ortu' => [
				'type'           => 'INT',
				'constraint'     => 11,
			],
			'nis' => [
				'type'           => 'VARCHAR',
				'constraint'     => 255,
				'unique'	     => true,
			],
			'nama' => [
				'type'           => 'VARCHAR',
				'constraint'     => 255,
			],
			'tgllahir' => [
				'type'           => 'VARCHAR',
				'constraint'     => 255,
			],
			'tmplahir' => [
				'type'           => 'TEXT',
			],
			'jnskel' => [
				'type'           => 'VARCHAR',
				'constraint'     => 255,
			],
			'agama' => [
				'type'           => 'VARCHAR',
				'constraint'     => 255,
			],
			'alamat' => [
				'type'           => 'VARCHAR',
				'constraint'     => 255,
			],
			'no_telp' => [
				'type'           => 'VARCHAR',
				'constraint'     => 255,
				'unique'	     => true,
			],
			'kelas' => [
				'type'           => 'VARCHAR',
				'constraint'     => 255,
			],
			'tahun' => [
				'type'           => 'VARCHAR',
				'constraint'     => 255,
			],
			'anak_ke' => [
				'type'           => 'VARCHAR',
				'constraint'     => 255,
			],
			'foto' => [
				'type'           => 'VARCHAR',
				'constraint'     => 255,

				'unique'	     => true,
			],
			'created_at DATETIME NOT NULL default CURRENT_TIMESTAMP',
			'updated_at DATETIME NOT NULL default CURRENT_TIMESTAMP',
			'deleted_at DATETIME NULL default NULL'
		]);

		// Membuat primary key
		$this->forge->addKey('id_siswa', TRUE);
		// Membuat tabel news
		$this->forge->createTable('siswa', TRUE);
	}

	public function down()
	{
		$this->forge->dropTable('siswa');
	}
}
