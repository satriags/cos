<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Guru extends Migration
{
	public function up()
	{
		// Membuat kolom/field untuk tabel news
		$this->forge->addField([
			'id_guru' => [
				'type'           => 'INT',
				'constraint'     => 11,
				'auto_increment' => true
			],
			'id_user' => [
				'type'           => 'INT',
				'constraint'     => 11,
			],
			'nama' => [
				'type'           => 'VARCHAR',
				'constraint'     => 255,
			],
			'wali_kelas' => [
				'type'           => 'VARCHAR',
				'constraint'     => 255,
			],
			'jnskel' => [
				'type'           => 'VARCHAR',
				'constraint'     => 255,
			],
			'mapel' => [
				'type'           => 'VARCHAR',
				'constraint'     => 255,
			],
			'no_telp' => [
				'type'           => 'VARCHAR',
				'constraint'     => 255,
				'unique'	     => true,
			],
			'jabatan' => [
				'type'           => 'VARCHAR',
				'constraint'     => 255,
			],
			'created_at DATETIME NOT NULL default CURRENT_TIMESTAMP',
			'updated_at DATETIME NOT NULL default CURRENT_TIMESTAMP',
			'deleted_at DATETIME NULL default NULL'
		]);

		// Membuat primary key
		$this->forge->addKey('id_guru', TRUE);
		// Membuat tabel news
		$this->forge->createTable('guru', TRUE);
	}

	public function down()
	{
		$this->forge->dropTable('guru');
	}
}
