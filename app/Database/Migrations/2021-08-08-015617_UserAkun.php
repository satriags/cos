<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class User extends Migration
{
	public function up()
	{
		// Membuat kolom/field untuk tabel news
		$this->forge->addField([
			'id_user' => [
				'type'           => 'INT',
				'constraint'     => 11,
				'auto_increment' => true
			],
			'username'       => [
				'type'           => 'VARCHAR',
				'constraint'     => 255,
				'unique'	     => true,
			],
			'password' => [
				'type'           => 'VARCHAR',
				'constraint'     => 500,
			],
			'type' => [
				'type'           => 'ENUM("Admin","BK","Guru","Ortu")'
			],
			'created_at DATETIME NOT NULL default CURRENT_TIMESTAMP',
			'updated_at DATETIME NOT NULL default CURRENT_TIMESTAMP',
			'deleted_at DATETIME NULL default NULL'
		]);

		// Membuat primary key
		$this->forge->addKey('id_user', TRUE);
		// Membuat tabel news
		$this->forge->createTable('user', TRUE);
	}

	public function down()
	{
		$this->forge->dropTable('user');
	}
}
