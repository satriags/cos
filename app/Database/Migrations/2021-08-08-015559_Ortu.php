<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Ortu extends Migration
{
	public function up()
	{
		// Membuat kolom/field untuk tabel news
		$this->forge->addField([
			'id_ortu' => [
				'type'           => 'INT',
				'constraint'     => 11,
				'auto_increment' => true
			],
			'id_user' => [
				'type'           => 'INT',
				'constraint'     => 11,
			],
			'nama' => [
				'type'           => 'VARCHAR',
				'constraint'     => 255,
			],
			'no_telp' => [
				'type'           => 'VARCHAR',
				'constraint'     => 255,
				'unique'	     => true,
			],
			'alamat' => [
				'type'           => 'VARCHAR',
				'constraint'     => 255,
			],
			'pekerjaan' => [
				'type'           => 'VARCHAR',
				'constraint'     => 255,
			],
			'created_at DATETIME NOT NULL default CURRENT_TIMESTAMP',
			'updated_at DATETIME NOT NULL default CURRENT_TIMESTAMP',
			'deleted_at DATETIME NULL default NULL'
		]);

		// Membuat primary key
		$this->forge->addKey('id_ortu', TRUE);
		// Membuat tabel news
		$this->forge->createTable('ortu', TRUE);
	}

	public function down()
	{
		$this->forge->dropTable('ortu');
	}
}
