<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Chat extends Migration
{
	public function up()
	{
		// Membuat kolom/field untuk tabel news
		$this->forge->addField([
			'id_chat' => [
				'type'           => 'INT',
				'constraint'     => 11,
				'auto_increment' => true
			],
			'id_user' => [
				'type'           => 'INT',
				'constraint'     => 11,
			],
			'id_pelanggaran' => [
				'type'           => 'INT',
				'constraint'     => 11,
			],
			'chat'       => [
				'type'           => 'TEXT'
			],
			'created_at DATETIME NOT NULL default CURRENT_TIMESTAMP',
			'updated_at DATETIME NOT NULL default CURRENT_TIMESTAMP',
			'deleted_at DATETIME NULL default NULL'
		]);

		// Membuat primary key
		$this->forge->addKey('id_chat', TRUE);
		// Membuat tabel news
		$this->forge->createTable('chat', TRUE);
	}

	public function down()
	{
		$this->forge->dropTable('chat');
	}
}
