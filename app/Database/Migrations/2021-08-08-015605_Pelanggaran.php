<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Pelanggaran extends Migration
{
	public function up()
	{
		// Membuat kolom/field untuk tabel news
		$this->forge->addField([
			'id_pelanggaran' => [
				'type'           => 'INT',
				'constraint'     => 11,
				'auto_increment' => true
			],
			'id_siswa' => [
				'type'           => 'INT',
				'constraint'     => 11,
			],
			'id_guru' => [
				'type'           => 'INT',
				'constraint'     => 11,
			],
			'id_ortu' => [
				'type'           => 'INT',
				'constraint'     => 11,
			],
			'judul' => [
				'type'           => 'VARCHAR',
				'constraint'     => 255,
			],
			'kategori' => [
				'type'           => 'VARCHAR',
				'constraint'     => 255,
			],
			'tanggal' => [
				'type'           => 'VARCHAR',
				'constraint'     => 255,
			],
			'deskripsi' => [
				'type'           => 'TEXT',
			],
			'status' => [
				'type'           => 'VARCHAR',
				'constraint'     => 255,
			],
			'created_at DATETIME NOT NULL default CURRENT_TIMESTAMP',
			'updated_at DATETIME NOT NULL default CURRENT_TIMESTAMP',
			'deleted_at DATETIME NULL default NULL'
		]);

		// Membuat primary key
		$this->forge->addKey('id_pelanggaran', TRUE);
		// Membuat tabel news
		$this->forge->createTable('pelanggaran', TRUE);
	}

	public function down()
	{
		$this->forge->dropTable('pelanggaran');
	}
}
