<?php

namespace App\Models;

use CodeIgniter\Model;

class SiswaModel extends Model
{
    protected $table = "siswa";
    // jika id berbeda maka di masukkan disini

    // jika ada created at maka di TRUE 
    protected $useTimestamps = true;
    protected $primaryKey = 'id_siswa';

    protected $useSoftDeletes = false;
    protected $deletedField  = 'deleted_at';

    protected $allowedFields = [
        'id_ortu',
        'nis',
        'nama',
        'tgllahir',
        'tmplahir',
        'jnskel',
        'agama',
        'alamat',
        'no_telp',
        'kelas',
        'tahun',
        'anak_ke',
        'foto',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
