<?php

namespace App\Models;

use CodeIgniter\Model;

class ChatModel extends Model
{

    protected $table = "chat";
    // jika id berbeda maka di masukkan disini

    // jika ada created at maka di TRUE 
    protected $useTimestamps = true;
    protected $primaryKey = 'id_chat';

    protected $useSoftDeletes = false;
    protected $deletedField  = 'deleted_at';

    protected $allowedFields = [
        'id_user',
        'id_pelanggaran',
        'chat',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
