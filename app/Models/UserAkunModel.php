<?php

namespace App\Models;

use CodeIgniter\Model;

class UserAkunModel extends Model
{
    protected $table = "user";
    // jika id berbeda maka di masukkan disini

    // jika ada created at maka di TRUE 
    protected $useTimestamps = true;
    protected $primaryKey = 'id_user';

    protected $useSoftDeletes = false;
    protected $deletedField  = 'deleted_at';

    protected $allowedFields = [
        'username',
        'password',
        'type',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
