<?php

namespace App\Models;

use CodeIgniter\Model;

class PelanggaranModel extends Model
{
    protected $table = "pelanggaran";
    // jika id berbeda maka di masukkan disini

    // jika ada created at maka di TRUE 
    protected $useTimestamps = true;
    protected $primaryKey = 'id_pelanggaran';

    protected $useSoftDeletes = false;
    protected $deletedField  = 'deleted_at';

    protected $allowedFields = [
        'id_siswa',
        'id_guru',
        'id_ortu',
        'judul',
        'kategori',
        'tanggal',
        'deskripsi',
        'status',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
