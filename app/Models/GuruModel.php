<?php

namespace App\Models;

use CodeIgniter\Model;

class GuruModel extends Model
{
    protected $table = "guru";
    // jika id berbeda maka di masukkan disini

    // jika ada created at maka di TRUE 
    protected $useTimestamps = true;
    protected $primaryKey = 'id_guru';

    protected $useSoftDeletes = false;
    protected $deletedField  = 'deleted_at';

    protected $allowedFields = [
        'id_user',
        'nama',
        'wali_kelas',
        'jnskel',
        'mapel',
        'no_telp',
        'jabatan',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
