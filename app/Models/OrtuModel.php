<?php

namespace App\Models;

use CodeIgniter\Model;

class OrtuModel extends Model
{
    protected $table = "ortu";
    // jika id berbeda maka di masukkan disini

    // jika ada created at maka di TRUE 
    protected $useTimestamps = true;
    protected $primaryKey = 'id_ortu';

    protected $useSoftDeletes = false;
    protected $deletedField  = 'deleted_at';

    protected $allowedFields = [
        'id_user',
        'nama',
        'no_telp',
        'alamat',
        'pekerjaan',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
